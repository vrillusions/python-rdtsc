#include <Python.h>
 
static PyObject* rdtsc(PyObject* self, PyObject* args)
{
     unsigned a, d;
     asm volatile("rdtsc" : "=a" (a), "=d" (d));

     return Py_BuildValue("K", (((unsigned long long) a) | (((unsigned long long) d) << 32)));
}
 
static PyMethodDef Methods[] =
{
     {"rdtsc", rdtsc, METH_NOARGS, "rdtsc"},
     {NULL, NULL, 0, NULL}
};
 
PyMODINIT_FUNC
 
initrdtsc(void)
{
     (void) Py_InitModule("rdtsc", Methods);
}

